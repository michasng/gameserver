import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppGateway } from './app.gateway';
import { AppService } from './app.service';
import { Client } from './client/client.entity';
import { ClientModule } from './client/client.module';
import { SpyClient } from './games/spy/spy-client.entity';
import { SpyGame } from './games/spy/spy-game.entity';
import { SpyModule } from './games/spy/spy.module';
import { Chat } from './games/util/chat/chat.entity';
import { ChatModule } from './games/util/chat/chat.module';
import { CommandModule } from './games/util/command/command.module';
import { Lobby } from './lobby/lobby.entity';
import { LobbyModule } from './lobby/lobby.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './db.sql',
      entities:
        [
          Client,
          Lobby,
          Chat,
          SpyGame,
          SpyClient,
        ],
      synchronize: true,
    }),
    ClientModule,
    LobbyModule,
    CommandModule,
    ChatModule,
    SpyModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    AppGateway,
  ],
})
export class AppModule { }
