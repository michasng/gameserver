import { ArgumentsHost, Catch } from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';
import WebSocket = require('ws');

@Catch()
export class WsExceptionsFilter extends BaseWsExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    super.catch(exception, host);
    // console.log(exception);
    const socket = host.switchToWs().getClient<WebSocket>();
    const reply = {
      error: exception
    }
    socket.send(JSON.stringify(reply), (err) => {
      if (err !== undefined)
        console.error("Could not reply with exception", err)
    });
  }
}
