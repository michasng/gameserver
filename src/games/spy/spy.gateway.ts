import { UseFilters } from '@nestjs/common';
import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Client } from 'src/client/client.entity';
import { ClientGateway } from 'src/client/client.gateway';
import { ClientService } from 'src/client/client.service';
import { SpyGuessEvent, SpyHintEvent, SpyVoteEvent, SPY_GUESS_EVENT, SPY_HINT_EVENT, SPY_INIT_GAME_EVENT, SPY_QUIT_GAME_EVENT, SPY_START_EVENT, SPY_STATE_EVENT, SPY_VOTE_EVENT } from 'src/games/spy/spy.events';
import { WsExceptionsFilter } from 'src/ws-exception.filter';
import { Server } from 'ws';
import { ChatDto, ChatMessageEvent } from '../util/chat/chat.events';
import { ChatGateway } from '../util/chat/chat.gateway';
import { Command } from '../util/command/command';
import { CommandService } from '../util/command/command.service';
import { SpyGameService } from './spy-game.service';
import WebSocket = require('ws');

@WebSocketGateway(5000)
export class SpyGateway {

  @WebSocketServer()
  server: Server;

  constructor(
    private readonly spyGameService: SpyGameService,
    private readonly clientService: ClientService,
    private readonly clientGateway: ClientGateway,
    private readonly chatGateway: ChatGateway,
    private readonly commandService: CommandService,
  ) {
    this.clientService.subscribeToLeaveGame(async (client: Client) => {
      try {
        await this.spyGameService.removeClient(client.id, client.lobby.id);
      } catch (err) {
        console.error(err);
      }
    });

    this.commandService.subscribe("spy-score", async (command: Command) => {
      const clientId = this.clientService.getClientId(command.socket);
      const client = await this.clientService.findOne(clientId);
      try {
        this.spyGameService.assertIsLobbyLeader(client);
        const deltaScores = command.args.map(value => {
          const numValue = Number(value);
          if (isNaN(numValue))
            throw ("Argument is not a number: " + value);
          return numValue;
        });
        const scoreEvent = await this.spyGameService.giveScores(client.lobby.id, deltaScores);
        if (scoreEvent !== null)
          await this.clientGateway.broadcastToLobby(command.socket, scoreEvent);
      } catch (err) {
        console.error(err);
        return;
      }
    });
  }

  @SubscribeMessage(SPY_INIT_GAME_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleInitGame(
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    console.log('handle init game');
    const clientId = this.clientService.getClientId(socket);
    const initGameEvent = await this.spyGameService.create(clientId);
    await this.clientGateway.broadcastToLobby(socket, initGameEvent);
    await this.handleStartGame(socket); // automatically start the game immediately
  }

  @SubscribeMessage(SPY_START_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleStartGame(
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    console.log('handle start');
    const clientId = this.clientService.getClientId(socket);
    const clientEvents = await this.spyGameService.handleStart(clientId);
    await this.clientGateway.broadcastClientEvents(socket, clientEvents);
  }

  @SubscribeMessage(SPY_HINT_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleHint(
    @ConnectedSocket() socket: WebSocket,
    @MessageBody() data: SpyHintEvent,
  ): Promise<void> {
    console.log('handle hint', data);
    const clientId = this.clientService.getClientId(socket);
    const hintEvent = await this.spyGameService.handleHint(clientId, data.hint);
    await this.clientGateway.broadcastToLobby(socket, hintEvent);
    const chatMessageEvent = new ChatMessageEvent(new ChatDto(
      clientId,
      hintEvent.hint,
      null, // timestamp yet to be decided
      true,
    ));
    await this.chatGateway.handleSend(chatMessageEvent, socket);
  }

  @SubscribeMessage(SPY_VOTE_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleVote(
    @ConnectedSocket() socket: WebSocket,
    @MessageBody() data: SpyVoteEvent,
  ): Promise<void> {
    console.log('handle vote', data);
    const clientId = this.clientService.getClientId(socket);
    const events = await this.spyGameService.handleVote(clientId, data.voteId);

    await this.clientGateway.broadcastToLobby(socket, events.voteEvent);
    if (events.revealEvent !== null)
      await this.clientGateway.broadcastToLobby(socket, events.revealEvent);
    if (events.scoreEvent !== null)
      await this.clientGateway.broadcastToLobby(socket, events.scoreEvent);
  }

  @SubscribeMessage(SPY_GUESS_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleGuess(
    @ConnectedSocket() socket: WebSocket,
    @MessageBody() data: SpyGuessEvent,
  ): Promise<void> {
    console.log('handle guess', data);
    const clientId = this.clientService.getClientId(socket);
    const events = await this.spyGameService.handleGuess(clientId, data.guessIndex);
    await this.clientGateway.broadcastToLobby(socket, events.guessEvent);
    await this.clientGateway.broadcastToLobby(socket, events.scoreEvent);
  }

  @SubscribeMessage(SPY_QUIT_GAME_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleQuit(
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    console.log('handle quit game');
    const clientId = this.clientService.getClientId(socket);
    const event = await this.spyGameService.remove(clientId);
    await this.clientGateway.broadcastToLobby(socket, event);
  }

  @SubscribeMessage(SPY_STATE_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleState(
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    console.log('handle state');
    const clientId = this.clientService.getClientId(socket);
    const client = await this.clientService.findOne(clientId);
    const event = await this.spyGameService.handleState(client.lobby.id);
    await this.clientGateway.broadcastToLobby(socket, event);
  }

}
