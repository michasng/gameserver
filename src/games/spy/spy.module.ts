import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from 'src/client/client.module';
import { LobbyModule } from 'src/lobby/lobby.module';
import { ChatModule } from '../util/chat/chat.module';
import { CommandModule } from '../util/command/command.module';
import { SpyClient } from './spy-client.entity';
import { SpyClientService } from './spy-client.service';
import { SpyGame } from './spy-game.entity';
import { SpyGameService } from './spy-game.service';
import { SpyGateway } from './spy.gateway';

@Module({
  imports: [
    TypeOrmModule.forFeature([SpyGame, SpyClient]),
    LobbyModule,
    ClientModule,
    ChatModule,
    CommandModule,
  ],
  providers: [
    SpyGameService,
    SpyClientService,
    SpyGateway,
  ],
  controllers: [],
  exports: [
    TypeOrmModule,
  ],
})
export class SpyModule { }
