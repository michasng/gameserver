import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from 'src/client/client.entity';
import { ClientEvent } from 'src/client/client.gateway';
import { ClientService } from 'src/client/client.service';
import { categories } from 'src/games/spy/spy-categories.json';
import { LobbyService } from 'src/lobby/lobby.service';
import { Repository } from 'typeorm';
import { SpyClientService } from './spy-client.service';
import { SpyGame, SpyGamePhase } from './spy-game.entity';
import { CategoryDto, SpyClientDto, SpyGuessEvent, SpyHintEvent, SpyInitGameEvent, SpyQuitGameEvent, SpyRevealEvent, SpyScoreEvent, SpyStartEvent, SpyStateEvent, SpyVoteEvent } from './spy.events';

@Injectable()
export class SpyGameService {

  constructor(
    @InjectRepository(SpyGame) private readonly spyGameRepository: Repository<SpyGame>,
    private readonly clientService: ClientService,
    private readonly lobbyService: LobbyService,
    private readonly spyClientService: SpyClientService,
  ) { }

  async findOne(lobbyId: string, withClientsClient: boolean = false): Promise<SpyGame> {
    const relations = ['lobby', 'clients'];
    if (withClientsClient)
      relations.push('clients.client');
    const spyGame = await this.spyGameRepository.findOne(lobbyId, { relations });
    if (spyGame === undefined)
      throw `No SpyGame associated with lobby ${lobbyId}`;
    return spyGame;
  }

  async create(clientId: string): Promise<SpyInitGameEvent> {
    const client = await this.clientService.findOne(clientId);
    this.assertIsLobbyLeader(client);
    const lobbyId = client.lobby.id;
    const lobby = await this.lobbyService.findOne(lobbyId);
    const spyGameInit = {
      lobby,
      clients: [],
    } as Partial<SpyGame>;
    const spyGame = await this.spyGameRepository.save(new SpyGame(spyGameInit));
    console.log(`SpyGame created ${lobbyId}`);
    spyGame.clients = await Promise.all(
      lobby.clients.map(client => {
        return this.spyClientService.create(client.id, spyGame);
      }
      )
    );
    await this.spyGameRepository.save(spyGame);
    console.log(`SpyGame saved ${lobbyId}`);
    return new SpyInitGameEvent();
  }

  async handleStart(clientId: string): Promise<ClientEvent[]> {
    const client = await this.clientService.findOne(clientId);
    this.assertIsLobbyLeader(client);
    const lobbyId = client.lobby.id;
    let spyGame = await this.findOne(lobbyId, true);
    // fixed, but rotating client order
    const length = spyGame.clients.length;
    const offset = length - (spyGame.round % length);
    const clientOrderIndexes = spyGame.clients.map(c =>
      (c.client.ordinary + offset) % length
    );
    // console.log(`round: ${spyGame.round}, clientOrderIndexes: ${clientOrderIndexes.toString()}`);
    const clients: Array<SpyClientDto> = [];
    for (let i = 0; i < spyGame.clients.length; i++) {
      const client = spyGame.clients[i];
      client.orderIndex = clientOrderIndexes[i];
      client.voteId = null;
      client.hint = null;
      clients.push(new SpyClientDto(
        client.id,
        client.orderIndex,
        client.hint,
        client.voteId,
      ));
    }
    // random spy
    const spyIndex = Math.floor(Math.random() * clients.length);
    spyGame.spyId = spyGame.clients[spyIndex].id;
    spyGame.phase = SpyGamePhase.HINT;
    spyGame.turnClientIndex = 0;
    // random category
    let categoryIndex;
    let category;
    do {
      categoryIndex = Math.floor(Math.random() * categories.length);
      category = categories[categoryIndex];
    } while (category.words.length < 16) // only use complete categories, none below 16 words
    // take 16 random words from it
    this.shuffle(category.words);
    // without overriding the original category.words reference
    category = new CategoryDto(category.name, category.words.slice(0, 16));
    spyGame.categoryName = category.name;
    spyGame.categoryWords = category.words;
    // random secret
    spyGame.secretIndex = Math.floor(Math.random() * category.words.length);
    spyGame = await this.spyGameRepository.save(spyGame);
    console.log(`SpyGame started ${lobbyId}`);

    const clientEvents: ClientEvent[] = [];
    for (const client of spyGame.clients) {
      let event;
      if (client.id == spyGame.spyId)
        event = new SpyStartEvent(clients, spyGame.spyId, category, -1);
      else
        event = new SpyStartEvent(clients, null, category, spyGame.secretIndex);
      clientEvents.push({ event, clientId: client.id } as ClientEvent);
    }
    return clientEvents;
  }

  async handleHint(clientId: string, hint: string): Promise<SpyHintEvent> {
    const client = await this.clientService.findOne(clientId);
    const lobbyId = client.lobby.id;
    const spyGame = await this.findOne(lobbyId);
    // check phase
    this.assertPhaseMatch(spyGame.phase, SpyGamePhase.HINT);
    // check turnClient
    const turnClient = spyGame.clients.find(c => c.orderIndex === spyGame.turnClientIndex);
    this.assertClientsMatch(clientId, turnClient.id);
    // modify game
    turnClient.hint = hint;
    // next turn
    spyGame.turnClientIndex++;
    if (spyGame.turnClientIndex === spyGame.clients.length) {
      spyGame.phase = SpyGamePhase.VOTE;
      spyGame.turnClientIndex = -1;
    }
    // save and return event
    await this.spyGameRepository.save(spyGame);
    return new SpyHintEvent(clientId, hint);
  }

  async handleVote(clientId: string, clientVoteId: string):
    Promise<{
      voteEvent: SpyVoteEvent,
      revealEvent: SpyRevealEvent,
      scoreEvent: SpyScoreEvent
    }> {
    const client = await this.clientService.findOne(clientId);
    const lobbyId = client.lobby.id;
    const spyGame = await this.findOne(lobbyId);
    // check phase
    this.assertPhaseMatch(spyGame.phase, SpyGamePhase.VOTE);
    // check voteId
    const voteClient = spyGame.clients.find(c => c.id === clientVoteId);
    if (voteClient === undefined)
      throw `VoteId ${clientVoteId} is not a valid client`;
    // modify game
    const spyClient = spyGame.clients.find(c => c.id == clientId);
    spyClient.voteId = clientVoteId;
    // create vote event
    const voteEvent = new SpyVoteEvent(clientId, clientVoteId);
    // check if everybody has voted (at least once, although only the last one counts)
    let everybodyVoted = true;
    spyGame.clients.forEach(c => {
      if (c.voteId === null)
        everybodyVoted = false;
    });
    if (!everybodyVoted) {
      // save and return no events
      await this.spyGameRepository.save(spyGame);
      return {
        voteEvent,
        revealEvent: null,
        scoreEvent: null,
      };
    }

    // count up the votes
    const votes = {};
    spyGame.clients.forEach(c => {
      if (votes[c.voteId] === undefined)
        votes[c.voteId] = 1;
      else
        votes[c.voteId] += 1;
    });
    // find max voted for
    let max = 0;
    for (const [id, value] of Object.entries(votes)) {
      if (value > max) {
        max = value as number;
        spyGame.voteId = id;
      }
    }
    // check if max is a duplicate
    let found = 0;
    for (const numVotes of Object.values(votes)) {
      if (numVotes == max)
        found++;
    }
    // check if it's a draw
    if (found > 1) {
      // player 1 decides
      const player1 = spyGame.clients.find(c => c.orderIndex === 0);
      spyGame.voteId = player1.voteId;
    }
    let secretIndex = -1;
    let scoreEvent = null;
    const votedCorrectly = (spyGame.spyId == spyGame.voteId);
    if (votedCorrectly) {
      spyGame.phase = SpyGamePhase.GUESS;
    } else {
      // reveal the secret to the spy
      secretIndex = spyGame.secretIndex;
      // end the round
      spyGame.phase = SpyGamePhase.STANDBY;
      spyGame.round++;
      // ascending in-place sorting
      spyGame.clients.sort((c1, c2) => c1.orderIndex - c2.orderIndex);
      // determine scores
      spyGame.clients.forEach(c => {
        const isSpy = (c.id == spyGame.spyId);
        c.score += isSpy ? 2 : 0;
      });
      const clientScores = spyGame.clients.map(c => c.score);
      scoreEvent = new SpyScoreEvent(clientScores);
    }
    // reveal the spy
    const revealEvent = new SpyRevealEvent(spyGame.spyId, spyGame.voteId, secretIndex);
    // save and return event
    await this.spyGameRepository.save(spyGame);
    return { voteEvent, revealEvent, scoreEvent };
  }

  async handleGuess(clientId: string, guessIndex: number):
    Promise<{
      guessEvent: SpyGuessEvent,
      scoreEvent: SpyScoreEvent,
    }> {
    const client = await this.clientService.findOne(clientId);
    const lobbyId = client.lobby.id;
    const spyGame = await this.findOne(lobbyId);
    // check phase
    this.assertPhaseMatch(spyGame.phase, SpyGamePhase.GUESS);
    // check for spy-role
    this.assertClientsMatch(clientId, spyGame.spyId);
    // check if the guess was correct
    spyGame.guessIndex = guessIndex;
    const guessedCorrectly = (spyGame.secretIndex == guessIndex);
    // end the round
    spyGame.phase = SpyGamePhase.STANDBY;
    spyGame.round++;
    // ascending in-place sorting
    spyGame.clients.sort((c1, c2) => c1.orderIndex - c2.orderIndex);
    // determine scores
    spyGame.clients.forEach(c => {
      const isSpy = (c.id == spyGame.spyId);
      c.score += isSpy ? (guessedCorrectly ? 1 : 0) : (guessedCorrectly ? 0 : 2);
    });
    const clientScores = spyGame.clients.map(c => c.score);
    // save and return event
    await this.spyGameRepository.save(spyGame);
    return {
      guessEvent: new SpyGuessEvent(spyGame.secretIndex, guessIndex),
      scoreEvent: new SpyScoreEvent(clientScores),
    };
  }

  async remove(clientId: string): Promise<SpyQuitGameEvent> {
    const client = await this.clientService.findOne(clientId);
    this.assertIsLobbyLeader(client);
    const lobbyId = client.lobby.id;
    const spyGame = await this.findOne(lobbyId);
    const event = new SpyQuitGameEvent();
    await this.spyGameRepository.remove(spyGame);
    console.log(`SpyGame removed ${lobbyId}`);
    return event;
  }

  // does not return a event, bc clients already have all the information to handle this case
  async removeClient(clientId: string, lobbyId: string) {
    // the client cannot be the leader or the only player in the lobby, bc the lobby still exists
    const spyGame = await this.findOne(lobbyId);
    const index = spyGame.clients.findIndex(spyClient => spyClient.id === clientId);
    const spyClient = spyGame.clients[index];
    this.spyClientService.remove(spyClient.id);
    // index must exist
    spyGame.clients.splice(index, 1);
    spyGame.phase = SpyGamePhase.STANDBY;
    await this.spyGameRepository.save(spyGame);
  }

  async handleState(lobbyId: string): Promise<SpyStateEvent> {
    const spyGame = await this.findOne(lobbyId);
    const category = new CategoryDto(spyGame.categoryName, spyGame.categoryWords);
    const clients = spyGame.clients.map(client => new SpyClientDto(
      client.id,
      client.orderIndex,
      client.hint,
      client.voteId,
    ));
    return new SpyStateEvent(
      clients,
      spyGame.spyId,
      spyGame.phase,
      spyGame.turnClientIndex,
      category,
      spyGame.secretIndex,
      spyGame.guessIndex,
    );
  }

  async giveScores(lobbyId: string, deltaScores: number[]): Promise<SpyScoreEvent> {
    let spyGame: SpyGame;
    spyGame = await this.findOne(lobbyId);
    if (deltaScores.length !== spyGame.clients.length) {
      console.log("Argument list does not match number of clients");
      return null;
    }
    const clientScores: number[] = [];
    spyGame.clients.sort((c1, c2) => c1.orderIndex - c2.orderIndex);
    spyGame.clients.forEach((spyClient) => {
      spyClient.score += deltaScores[spyClient.orderIndex];
      clientScores.push(spyClient.score);
    });
    await this.spyGameRepository.save(spyGame);
    return new SpyScoreEvent(clientScores);
  }

  assertClientsMatch(actualId: string, expectedId: string) {
    if (actualId !== expectedId)
      throw `Wrong client ${actualId} doesn't match ${expectedId}`;
  }

  assertPhaseMatch(actualPhase: string, expectedPhase: string) {
    if (actualPhase !== expectedPhase)
      throw `Current phase ${actualPhase} does not match ${expectedPhase}`;
  }

  assertIsLobbyLeader(client: Client) {
    if (!client.isLeader)
      throw 'Action can only be performed by lobby leader';
  }

  // Fisher-Yates (aka Knuth) Shuffle algorithm
  // https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
  public shuffle<T>(array: T[]): T[] {
    let currentIndex = array.length;
    let temporaryValue: T;
    let randomIndex: number;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

}
