import { WsEvent, WsMessage } from '../../ws-event';
import { SpyGamePhase } from './spy-game.entity';

// server: telling everyone to initialize their game
export const SPY_INIT_GAME_EVENT = 'spy-init-game';
// server: a new category with words, client roles and new secret
export const SPY_START_EVENT = 'spy-start';
// client/server: everyones hint to the secret
export const SPY_HINT_EVENT = 'spy-hint';
// client/server: everyones hint to who the spy is
export const SPY_VOTE_EVENT = 'spy-vote';
// server: tells everyone who the spy is
export const SPY_REVEAL_EVENT = 'spy-reveal';
// client/server: let the spy guess the word
export const SPY_GUESS_EVENT = 'spy-guess';
// server: giving out points
export const SPY_SCORE_EVENT = 'spy-score';
// server: declaring winner or just ending the game midway
export const SPY_QUIT_GAME_EVENT = 'spy-quit-game';

// client/server: send entire state of the game
export const SPY_STATE_EVENT = 'spy-state';

export class CategoryDto {
  constructor(
    public readonly name: string,
    public readonly words: string[],
  ) { }
}

export class SpyClientDto {
  constructor(
    public readonly id: string,
    public readonly orderIndex: number,
    public readonly hint: string,
    public readonly voteId: string,
  ) { }
}

export class SpyInitGameEvent extends WsEvent {
  constructor(
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_INIT_GAME_EVENT);
  }
}

export class SpyStartEvent extends WsEvent {
  constructor(
    public readonly clients: SpyClientDto[],
    public readonly spyId: string,
    public readonly category: CategoryDto,
    public readonly secretIndex: number,
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_START_EVENT);
  }
}

export class SpyHintEvent extends WsEvent {
  constructor(
    public readonly originClientId: string,
    public readonly hint: string,
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_HINT_EVENT);
  }
}

export class SpyVoteEvent extends WsEvent {
  constructor(
    public readonly originClientId: string,
    public readonly voteId: string,
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_VOTE_EVENT);
  }
}

export class SpyRevealEvent extends WsEvent {
  constructor(
    public readonly spyId: string,
    public readonly voteId: string,
    // is only set if spyId != voteId, otherwise -1
    public readonly secretIndex: number,
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_REVEAL_EVENT);
  }
}

export class SpyGuessEvent extends WsEvent {
  constructor(
    public readonly secretIndex: number,
    public readonly guessIndex: number,
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_GUESS_EVENT);
  }
}

export class SpyScoreEvent extends WsEvent {
  constructor(
    public readonly clientScores: number[],
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_SCORE_EVENT);
  }
}

export class SpyQuitGameEvent extends WsEvent {
  constructor(
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_QUIT_GAME_EVENT);
  }
}

export class SpyStateEvent extends WsEvent {
  constructor(
    public readonly clients: SpyClientDto[],
    public readonly spyId: string,
    public readonly phase: SpyGamePhase,
    public readonly turnClientIndex: number,
    public readonly category: CategoryDto,
    public readonly secretIndex: number,
    public readonly guessIndex: number,
  ) { super(); }
  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, SPY_STATE_EVENT);
  }
}
