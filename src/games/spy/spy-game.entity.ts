import { Lobby } from 'src/lobby/lobby.entity';
import { Column, Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { SpyClient } from './spy-client.entity';

export enum SpyGamePhase {
  HINT = 'hint',
  VOTE = 'vote',
  GUESS = 'guess',
  STANDBY = 'standby',
}

@Entity()
export class SpyGame {

  @OneToOne(type => Lobby, { cascade: true, onDelete: 'CASCADE', primary: true })
  @JoinColumn()
  lobby: Lobby;

  @OneToMany(type => SpyClient, client => client.game, { cascade: true })
  clients: SpyClient[];

  @Column({ nullable: true })
  spyId: string; // uses a string, instead of foreign key, to avoid circular references

  @Column({ nullable: true })
  voteId: string;

  @Column({ nullable: true })
  phase: SpyGamePhase;

  @Column({ nullable: true })
  turnClientIndex: number;

  @Column({ nullable: true })
  categoryName: string;

  @Column('text', { nullable: true })
  _categoryWords: string;
  get categoryWords(): string[] {
    return JSON.parse(this._categoryWords);
  }
  set categoryWords(value: string[]) {
    this._categoryWords = JSON.stringify(value);
  }

  @Column({ nullable: true })
  secretIndex: number

  @Column({ nullable: true })
  guessIndex: number;

  @Column({ default: 0 })
  round: number;

  constructor(init?: Partial<SpyGame>) {
    Object.assign(this, init);
  }

}
