import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientService } from 'src/client/client.service';
import { Repository } from 'typeorm';
import { SpyClient } from './spy-client.entity';
import { SpyGame } from './spy-game.entity';

@Injectable()
export class SpyClientService {

  constructor(
    @InjectRepository(SpyClient) private readonly spyClientRepository: Repository<SpyClient>,
    private readonly clientService: ClientService,
  ) { }

  async findOne(clientId: string): Promise<SpyClient> {
    const spyClient = await this.spyClientRepository.findOne(clientId, { relations: ['client'] });
    if (spyClient === undefined)
      throw `No SpyClient associated with client ${clientId}`;
    return spyClient;
  }

  async create(clientId: string, game: SpyGame): Promise<SpyClient> {
    const client = await this.clientService.findOne(clientId);
    const spyClientInit = {
      client,
      id: client.id,
      game,
    } as Partial<SpyClient>;
    let spyClient;
    try {
      spyClient = await this.spyClientRepository.save(new SpyClient(spyClientInit));
    } catch (e) {
      console.log('error', e);
      console.error(e);
      throw e;
    }
    console.log(`SpyClient created ${clientId}`);
    return spyClient;
  }

  async update(spyClient: SpyClient): Promise<SpyClient> {
    return await this.spyClientRepository.save(spyClient);
  }

  async remove(clientId: string): Promise<void> {
    const spyClient = await this.findOne(clientId);
    await this.spyClientRepository.remove(spyClient);
    console.log(`SpyClient removed ${clientId}`);
  }

}
