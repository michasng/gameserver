import { Client } from 'src/client/client.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';
import { SpyGame } from './spy-game.entity';

@Entity()
export class SpyClient {

  @PrimaryColumn()
  id: string; // uses client.id aswell

  @OneToOne(type => Client, { nullable: true, onDelete: 'SET NULL' })
  @JoinColumn()
  client: Client;

  @ManyToOne(type => SpyGame, game => game.clients, { onDelete: "CASCADE" })
  game: SpyGame;

  // this property changes every round, as opposed to client.ordinary
  @Column({ nullable: true })
  orderIndex: number;

  @Column({ default: 0 })
  score: number;

  @Column({ nullable: true })
  hint: string;

  @Column({ nullable: true })
  voteId: string;

  constructor(init?: Partial<SpyClient>) {
    Object.assign(this, init);
  }

}
