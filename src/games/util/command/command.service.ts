import { Injectable } from '@nestjs/common';
import { Subject } from 'rxjs';
import { Command } from './command';
import WebSocket = require('ws');

@Injectable()
export class CommandService {

  commandNames: string[];

  subjects: Subject<Command>[];

  constructor(
  ) {
    this.commandNames = [];
    this.subjects = [];
  }

  parse(message: string, socket: WebSocket) {
    // remove backslash, make lower-case and separate words
    const words = message.slice(1).toLowerCase().split(" ");
    const index = this.commandNames.indexOf(words[0]);
    if (index === -1) {
      console.log("got invalid command", words);
      return null;
    }
    const command = {
      name: words[0],
      args: words.slice(1),
      socket,
    } as Command;
    console.log("got command", command.name, command.args);
    this.subjects[index].next(command);
  }

  subscribe(name: string, next: (command: Command) => void) {
    console.log("subscribe command", name);
    let index = this.commandNames.indexOf(name);
    if (index === -1) {
      index = this.commandNames.length;
      this.commandNames.push(name);
      this.subjects.push(new Subject());
    }
    this.subjects[index].subscribe({
      next,
      error: err => console.error(err),
      complete: () => { },
    });
  }

}
