import { Module } from '@nestjs/common';
import { CommandService } from './command.service';

@Module({
  imports: [],
  providers: [
    CommandService,
  ],
  controllers: [],
  exports: [
    CommandService,
  ],
})
export class CommandModule { }
