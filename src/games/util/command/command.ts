import WebSocket = require('ws');

export interface Command {
  name: string;
  socket: WebSocket;
  args: string[];
}
