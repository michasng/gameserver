import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Chat } from './chat.entity';

@Injectable()
export class ChatService {

  constructor(
    @InjectRepository(Chat) private readonly chatRepository: Repository<Chat>,
  ) { }

  async findAll(lobbyId?: string): Promise<Chat[]> {
    if (lobbyId) {
      return await this.chatRepository.find({
        where: [{ lobby: { id: lobbyId } }],
        relations: ['client']
      });
    }
    return await this.chatRepository.find({ relations: ['client'] });
  }

  async findOne(id: string): Promise<Chat> {
    const chat = await this.chatRepository.findOne({
      where: [{ id }],
      relations: ['client'],
    });
    if (chat === undefined)
      throw this.notFoundMessage(id);
    return chat;
  }

  async create(init: Partial<Chat>): Promise<Chat> {
    const chat = await this.chatRepository.save(new Chat(init));
    console.log(`Chat created ${chat.id}`);
    return chat;
  }

  async remove(id: string): Promise<void> {
    const chat = await this.chatRepository.findOne(id);
    await this.chatRepository.remove(chat);
    console.log(`Chat removed ${id}`);
  }

  async clear(lobbyId?: string): Promise<void> {
    if (lobbyId) {
      console.log('clearing chats of lobby', lobbyId);
      const chats = await this.findAll(lobbyId);
      for (let chat of chats)
        await this.chatRepository.remove(chat);
    } else {
      console.log('clearing chats');
      await this.chatRepository.clear();
    }
  }

  private notFoundMessage(id: string): string {
    return `Could not find chat with id ${id}`;
  }

}
