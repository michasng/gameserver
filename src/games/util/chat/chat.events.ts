import { WsEvent, WsMessage } from 'src/ws-event';

export const CHAT_LIST_EVENT = 'chat-list';
export const CHAT_MESSAGE_EVENT = 'chat-message';

export class ChatDto {
  constructor(
    public readonly clientId: string,
    public readonly text: string,
    public readonly timestamp: number,
    public readonly highlighted: boolean,
  ) { }
}

export class ChatListEvent extends WsEvent {

  constructor(
    public readonly messages: ChatDto[],
  ) { super(); }

  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, CHAT_LIST_EVENT);
  }

}

export class ChatMessageEvent extends WsEvent {

  constructor(
    public readonly message: ChatDto,
  ) { super(); }

  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, CHAT_MESSAGE_EVENT);
  }

}
