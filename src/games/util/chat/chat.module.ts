import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from 'src/client/client.module';
import { CommandModule } from '../command/command.module';
import { Chat } from './chat.entity';
import { ChatGateway } from './chat.gateway';
import { ChatService } from './chat.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Chat]),
    ClientModule,
    CommandModule,
  ],
  providers: [
    ChatService,
    ChatGateway,
  ],
  controllers: [],
  exports: [
    TypeOrmModule,
    ChatService,
    ChatGateway,
  ],
})
export class ChatModule { }
