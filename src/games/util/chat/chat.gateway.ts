import { UseFilters } from '@nestjs/common';
import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { ClientGateway } from 'src/client/client.gateway';
import { ClientService } from 'src/client/client.service';
import { ChatDto, ChatListEvent, ChatMessageEvent, CHAT_LIST_EVENT, CHAT_MESSAGE_EVENT } from 'src/games/util/chat/chat.events';
import { WsMessage } from 'src/ws-event';
import { WsExceptionsFilter } from 'src/ws-exception.filter';
import { Server } from 'ws';
import { CommandService } from '../command/command.service';
import { Chat } from './chat.entity';
import { ChatService } from './chat.service';
import WebSocket = require('ws');

@WebSocketGateway(5000)
export class ChatGateway {

  @WebSocketServer()
  server: Server;

  constructor(
    private readonly chatService: ChatService,
    private readonly clientService: ClientService,
    private readonly clientGateway: ClientGateway,
    private readonly commandService: CommandService,
  ) { }

  @SubscribeMessage(CHAT_LIST_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleList(
    @ConnectedSocket() socket: WebSocket,
  ): Promise<WsMessage> {
    const clientId = this.clientService.getClientId(socket);
    if (clientId === undefined)
      throw 'No client associated with this socket';
    const client = await this.clientService.findOne(clientId);
    const chats = await this.chatService.findAll(client.lobby.id);
    const messages: ChatDto[] = chats.map(
      chat => new ChatDto(
        chat.client ? chat.client.id : null,
        chat.text, chat.timestamp, false)
    );
    const event = new ChatListEvent(messages);
    return event.create(true);
  }

  @SubscribeMessage(CHAT_MESSAGE_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleSend(
    @MessageBody() data: ChatMessageEvent,
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    const text = data.message.text;
    const clientId = this.clientService.getClientId(socket);
    if (clientId === undefined)
      throw 'No client associated with this socket';
    const client = await this.clientService.findOne(clientId);
    const lobby = client.lobby;
    const timestamp = Math.floor(new Date().getTime() / 1000);
    const highlighted = data.message.highlighted ? true : false; // in case it's undefined
    const chat = { client, lobby, text, timestamp, highlighted } as Partial<Chat>;
    await this.chatService.create(chat);

    const event = new ChatMessageEvent(new ChatDto(
      client.id, text, timestamp, highlighted,
    ));
    this.clientGateway.broadcastToLobby(socket, event);

    if (text.charAt(0) === "/")
      this.commandService.parse(text, socket);
  }

}
