import { Client } from 'src/client/client.entity';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { Lobby } from 'src/lobby/lobby.entity';

@Entity()
export class Chat {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Client, { nullable: true, onDelete: "SET NULL" })
  client: Client;

  @ManyToOne(type => Lobby, { onDelete: "CASCADE" })
  lobby: Lobby;

  @Column()
  text: string;

  @Column()
  timestamp: number;

  @Column()
  highlighted: boolean;

  constructor(init?: Partial<Chat>) {
    Object.assign(this, init);
  }

}
