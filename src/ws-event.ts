
export class WsMessage {

  constructor(public event: string, public data: WsEvent) { }

}

export abstract class WsEvent {

  public isOrigin: boolean;

  abstract create(isOrigin: boolean): WsMessage;

  superCreate(isOrigin: boolean, name: string): WsMessage {
    this.isOrigin = isOrigin;
    return new WsMessage(name, this);
  }

}

