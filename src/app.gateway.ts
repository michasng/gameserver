import { MessageBody, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'ws';
import { WsEvent } from './ws-event';
import WebSocket = require('ws');

export function broadcast(originSocket: WebSocket, sockets: Set<WebSocket> | Array<WebSocket>, ...events: WsEvent[]) {
  if (originSocket == undefined)
    throw 'Invalid socket';
  sockets.forEach(socket => {
    for (const event of events) {
      const message = event.create(socket === originSocket);
      // console.log('sending message', (socket != undefined), message);
      socket.send(JSON.stringify(message));
    }
  });
}

@WebSocketGateway(5000)
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {

  @WebSocketServer()
  server: Server;

  constructor(
  ) { }

  handleConnection(socket: WebSocket) {
    console.log('connect client');
  }

  handleDisconnect(socket: WebSocket) {
    console.log('disconnect client');
  }

  @SubscribeMessage('echo')
  async handleEvent(
    @MessageBody() data: any,
  ): Promise<any> {
    console.log('data', data);
    return {
      event: 'echo',
      data: data,
    };
  }

}
