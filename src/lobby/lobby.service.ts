import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Lobby } from './lobby.entity';

@Injectable()
export class LobbyService implements OnModuleInit {

  constructor(
    @InjectRepository(Lobby) private readonly lobbyRepository: Repository<Lobby>,
  ) { }

  async onModuleInit(): Promise<void> {
    try {
      await this.clear();
    } catch (err) {
      console.error(err);
    }
  }

  async findAll(): Promise<Lobby[]> {
    const lobbies = await this.lobbyRepository.find({ relations: ['clients'] });
    for (const lobby of lobbies)
      lobby.clients.sort((c1, c2) => c1.ordinary - c2.ordinary);
    return lobbies;
  }

  async findOne(id: string): Promise<Lobby> {
    const lobby = await this.lobbyRepository.findOne({
      where: [{ id }],
      relations: ['clients'],
    });
    if (lobby === undefined)
      throw this.notFoundMessage(id);
    lobby.clients.sort((c1, c2) => c1.ordinary - c2.ordinary);
    return lobby;
  }

  async create(): Promise<Lobby> {
    const lobby = await this.lobbyRepository.save(new Lobby());
    console.log(`Lobby created ${lobby.id}`);
    return lobby;
  }

  async remove(id: string): Promise<void> {
    const lobby = await this.lobbyRepository.findOne(id);
    await this.lobbyRepository.remove(lobby);
    console.log(`Lobby removed ${id}`);
  }

  async clear(): Promise<void> {
    console.log('clearing lobbies');
    await this.lobbyRepository.clear();
  }

  private notFoundMessage(id: string): string {
    return `Could not find lobby with id ${id}`;
  }

}
