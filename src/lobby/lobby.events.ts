import { WsEvent, WsMessage } from 'src/ws-event';

export const LOBBY_LIST_EVENT = 'lobby-list';
export const LOBBY_CREATE_EVENT = 'lobby-create';
export const LOBBY_JOIN_EVENT = 'lobby-join';
export const LOBBY_LEAVE_EVENT = 'lobby-leave';

export class LobbyDto {
  constructor(
    public readonly id: string,
    public readonly clients: ClientDto[],
  ) { }
}

export class ClientDto {
  constructor(
    public readonly id: string,
    public readonly isLeader: boolean,
    public readonly displayName: string,
  ) { }
}

export class LobbyListEvent extends WsEvent {

  constructor(
    public readonly lobbies: LobbyDto[],
  ) { super(); }

  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, LOBBY_LIST_EVENT);
  }

}

export class LobbyCreateEvent extends WsEvent {

  constructor(
    public readonly client: ClientDto,
  ) { super(); }

  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, LOBBY_CREATE_EVENT);
  }

}

export class LobbyJoinEvent extends WsEvent {

  constructor(
    public readonly lobbyId: string,
    public readonly client: ClientDto,
  ) { super(); }

  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, LOBBY_JOIN_EVENT);
  }

}

export class LobbyLeaveEvent extends WsEvent {

  constructor(
    public readonly lobbyId: string,
    public readonly clientId: string,
  ) { super(); }

  create(isOrigin: boolean): WsMessage {
    return this.superCreate(isOrigin, LOBBY_LEAVE_EVENT);
  }

}
