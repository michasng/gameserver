import { Client } from 'src/client/client.entity';
import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Lobby {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(type => Client, client => client.lobby, { cascade: true })
  clients: Client[];

  constructor(init?: Partial<Lobby>) {
    Object.assign(this, init);
  }

}
