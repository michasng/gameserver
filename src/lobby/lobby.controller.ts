import { Controller, Delete, Get, HttpException, HttpStatus, Param, Post } from '@nestjs/common';
import { Lobby } from './lobby.entity';
import { LobbyService } from './lobby.service';

@Controller('lobbies')
export class LobbyController {

  constructor(private readonly lobbyService: LobbyService) { }

  @Get()
  // @Header('Content-Type', 'application/json')
  async findAll(): Promise<Lobby[]> {
    return this.lobbyService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Lobby> {
    const lobby = await this.lobbyService.findOne(id);
    if (lobby === undefined)
      throw new HttpException(`Could not find lobby with id ${id}`, HttpStatus.NOT_FOUND);
    return lobby;
  }

  @Post()
  async create(): Promise<Lobby> {
    return this.lobbyService.create();
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    const lobby = await this.lobbyService.remove(id);
    if (lobby === undefined)
      throw new HttpException(`Could not find lobby with id ${id}`, HttpStatus.NOT_FOUND);
  }

}
