import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LobbyController } from './lobby.controller';
import { Lobby } from './lobby.entity';
import { LobbyService } from './lobby.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Lobby]),
  ],
  providers: [
    LobbyService,
  ],
  controllers: [
    LobbyController,
  ],
  exports: [
    TypeOrmModule,
    LobbyService,
  ],
})
export class LobbyModule { }
