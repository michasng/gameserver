import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Subject, Subscription } from 'rxjs';
import { LobbyService } from 'src/lobby/lobby.service';
import { Repository } from 'typeorm';
import { Client } from './client.entity';
import WebSocket = require('ws');

@Injectable()
export class ClientService {

  private socketToClientId: Map<WebSocket, string>;
  private clientIdToSocket: Map<string, WebSocket>;
  // sockets can be locked on creation
  private lockedSockets: Map<WebSocket, boolean>;
  // notifies observers when a client leaves the lobby, but the lobby still exists
  private leaveGameSubject: Subject<Client>;

  constructor(
    @InjectRepository(Client) private readonly clientRepository: Repository<Client>,
    private readonly lobbyService: LobbyService,
  ) {
    this.socketToClientId = new Map();
    this.clientIdToSocket = new Map();
    this.lockedSockets = new Map();
    this.leaveGameSubject = new Subject();
  }

  async findAll(lobbyId?: string): Promise<Client[]> {
    if (lobbyId) {
      return await this.clientRepository.find({
        where: [{ lobby: { id: lobbyId } }],
      });
    }
    return await this.clientRepository.find({ relations: ['lobby'] });
  }

  async findOne(id: string, withLobbyClients: boolean = false): Promise<Client> {
    const relations = ['lobby'];
    if (withLobbyClients)
      relations.push('lobby.clients');
    const client = await this.clientRepository.findOne(id, { relations });
    if (client === undefined)
      throw this.notFoundMessage(id);
    if (withLobbyClients)
      client.lobby.clients.sort((c1, c2) => c1.ordinary - c2.ordinary);
    return client;
  }

  async create(socket: WebSocket, init?: Partial<Client>): Promise<Client> {
    // if the socket already has an associated client
    const oldClientId = this.getClientId(socket);
    if (oldClientId !== undefined) {
      // have the oldClient leave the lobby
      const oldClient = await this.findOne(oldClientId);
      await this.remove(oldClient);
    }

    const client = await this.clientRepository.save(new Client(init));

    this.socketToClientId.set(socket, client.id);
    this.clientIdToSocket.set(client.id, socket);
    this.lockedSockets.delete(socket);

    console.log(`Client created ${client.id}`);
    return client;
  }

  async remove(client: Client): Promise<void> {
    const id = client.id; // client is set to undefined after removing
    await this.clientRepository.remove(client);
    console.log(`Client removed ${id}`);

    // remove from maps
    const socket = this.getSocket(id);
    this.clientIdToSocket.delete(id);
    this.socketToClientId.delete(socket);
    this.unlockSocket(socket);

    // remove the lobby if empty
    const lobby = await this.lobbyService.findOne(client.lobby.id);
    if (lobby.clients.length === 0) {
      await this.lobbyService.remove(lobby.id);
      return;
    }

    // otherwise fix the ordinaries
    for (const client of lobby.clients) {
      client.ordinary--;
      await this.clientRepository.save(client);
    }

    // notify observers
    client = new Client({id, lobby}); // dummy object, don't save it!
    this.leaveGameSubject.next(client);
  }

  public lockSocket(socket: WebSocket): void {
    if (this.lockedSockets.has(socket))
      throw 'Socket is locked';
    this.lockedSockets.set(socket, true);
  }

  public unlockSocket(socket: WebSocket): void {
    this.lockedSockets.delete(socket);
  }

  public getClientId(socket: WebSocket): string {
    return this.socketToClientId.get(socket);
  }

  public getSocket(clientId: string): WebSocket {
    return this.clientIdToSocket.get(clientId);
  }

  public subscribeToLeaveGame(observer: (client: Client) => void): Subscription {
    return this.leaveGameSubject.subscribe({
      next: observer,
      error: err => console.error(err),
      complete: () => { },
    });
  }

  private notFoundMessage(id: string): string {
    return `Could not find client with id ${id}`;
  }

}
