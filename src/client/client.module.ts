import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LobbyModule } from 'src/lobby/lobby.module';
import { Client } from './client.entity';
import { ClientGateway } from './client.gateway';
import { ClientService } from './client.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Client]),
    LobbyModule,
  ],
  providers: [
    ClientService,
    ClientGateway,
  ],
  controllers: [],
  exports: [
    TypeOrmModule,
    ClientService,
    ClientGateway,
  ],
})
export class ClientModule { }
