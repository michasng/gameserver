import { Lobby } from 'src/lobby/lobby.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Client {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  displayName: string;

  @ManyToOne(type => Lobby, lobby => lobby.clients, { onDelete: "CASCADE" })
  lobby: Lobby;

  // keeps the index of the client in the lobby
  @Column()
  ordinary: number;

  @Column()
  isLeader: boolean;

  constructor(init?: Partial<Client>) {
    Object.assign(this, init);
  }
}
