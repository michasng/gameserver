import { UseFilters } from '@nestjs/common';
import { ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { broadcast } from 'src/app.gateway';
import { Client } from 'src/client/client.entity';
import { ClientService } from 'src/client/client.service';
import { Lobby } from 'src/lobby/lobby.entity';
import { ClientDto, LobbyCreateEvent, LobbyDto, LobbyJoinEvent, LobbyLeaveEvent, LobbyListEvent, LOBBY_CREATE_EVENT, LOBBY_JOIN_EVENT, LOBBY_LEAVE_EVENT, LOBBY_LIST_EVENT } from 'src/lobby/lobby.events';
import { WsEvent, WsMessage } from 'src/ws-event';
import { WsExceptionsFilter } from 'src/ws-exception.filter';
import { Server } from 'ws';
import { LobbyService } from '../lobby/lobby.service';
import WebSocket = require('ws');

@WebSocketGateway(5000)
export class ClientGateway implements OnGatewayConnection, OnGatewayDisconnect {

  @WebSocketServer()
  server: Server;

  constructor(
    private readonly lobbyService: LobbyService,
    private readonly clientService: ClientService,
  ) { }

  async handleConnection(socket: WebSocket) {
    try {
      const event = await this.makeLobbyListEvent();
      socket.send(JSON.stringify(event.create(true)))
    } catch (err) {
      console.error(err);
      // dismiss
    }
  }

  async handleDisconnect(socket: WebSocket) {
    try {
      await this.handleLeave(socket);
    } catch (err) {
      console.error(err);
      // dismiss
    }
  }

  @SubscribeMessage(LOBBY_LIST_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleList(): Promise<WsMessage> {
    const event = await this.makeLobbyListEvent();
    return event.create(true);
  }

  private async makeLobbyListEvent(): Promise<LobbyListEvent> {
    const lobbies = await this.lobbyService.findAll();
    return new LobbyListEvent(
      lobbies.map<LobbyDto>(
        lobby => {
          return new LobbyDto(
            lobby.id,
            lobby.clients.map(client => new ClientDto(
              client.id,
              client.isLeader,
              client.displayName
            )),
          )
        }
      ),
    );
  }

  @SubscribeMessage(LOBBY_CREATE_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleCreate(
    @MessageBody() data: LobbyCreateEvent,
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    let clientId = this.clientService.getClientId(socket);
    if (clientId !== undefined)
      throw 'There is already a client associated with this socket';
    this.clientService.lockSocket(socket);

    let lobby = await this.lobbyService.create();

    try {
      await this.handleJoinImpl(socket, data.client.displayName, lobby, true);
    } catch (err) {
      await this.lobbyService.remove(lobby.id);
      throw err;
    }
  }

  @SubscribeMessage(LOBBY_JOIN_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleJoin(
    @MessageBody() data: LobbyJoinEvent,
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    let clientId = this.clientService.getClientId(socket);
    if (clientId !== undefined)
      throw 'There is already a client associated with this socket';
    this.clientService.lockSocket(socket);

    let lobby = await this.lobbyService.findOne(data.lobbyId);

    await this.handleJoinImpl(socket, data.client.displayName, lobby, false);
  }

  private async handleJoinImpl(socket: WebSocket, displayName: string, lobby: Lobby, isLeader: boolean): Promise<void> {
    // next ordinary within the lobby's array of clients
    const ordinary = lobby.clients === undefined ? 0 : lobby.clients.length;
    const clientPartial: Partial<Client> = {
      displayName,
      lobby,
      ordinary,
      isLeader,
    };
    const client = await this.clientService.create(socket, clientPartial);

    // re-fetch to get list of clients
    lobby = await this.lobbyService.findOne(client.lobby.id);
    const event = new LobbyJoinEvent(lobby.id, new ClientDto(client.id, client.isLeader, client.displayName));
    broadcast(socket, this.server.clients, event);
  }

  @SubscribeMessage(LOBBY_LEAVE_EVENT)
  @UseFilters(WsExceptionsFilter)
  async handleLeave(
    @ConnectedSocket() socket: WebSocket,
  ): Promise<void> {
    const clientId = this.clientService.getClientId(socket);
    if (clientId === undefined)
      throw 'No client associated with this socket';

    const client = await this.clientService.findOne(clientId, true);
    if (!client.isLeader) {
      const event = new LobbyLeaveEvent(client.lobby.id, client.id);
      await this.clientService.remove(client);
      broadcast(socket, this.server.clients, event);
    } else { // leaving leader terminates lobby for now
      for (let c of client.lobby.clients) {
        c.lobby = client.lobby; // lobby property is required to remove client
        const event = new LobbyLeaveEvent(c.lobby.id, c.id);
        const socket = this.clientService.getSocket(c.id);
        await this.clientService.remove(c);
        broadcast(socket, this.server.clients, event);
      }
    }
  }

  public async broadcastToLobby(originSocket: WebSocket, ...events: WsEvent[]) {
    console.log('broadcast to lobby', events);
    const clientId = this.clientService.getClientId(originSocket);
    const client = await this.clientService.findOne(clientId, true);
    const sockets = client.lobby.clients.map(
      client => this.clientService.getSocket(client.id)
    );
    // console.log('broadcast events', events, client.lobby.clients);
    broadcast(originSocket, sockets, ...events);
  }

  public async broadcastClientEvents(originSocket: WebSocket, clientEvents: ClientEvent[]) {
    console.log('broadcast client events', clientEvents);
    clientEvents.forEach(cE => {
      const socket = this.clientService.getSocket(cE.clientId);
      const message = cE.event.create(socket === originSocket);
      socket.send(JSON.stringify(message));
    });
  }

}

export interface ClientEvent {
  clientId: string;
  event: WsEvent;
}
